//
//  PhotoCollectionViewCell.swift
//  workshop
//
//  Created by Matěj Novák on 14/12/2016.
//  Copyright © 2016 Matěj Novák. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!

}
