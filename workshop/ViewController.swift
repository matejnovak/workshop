//
//  ViewController.swift
//  workshop
//
//  Created by Matěj Novák on 14/12/2016.
//  Copyright © 2016 Matěj Novák. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorageUI

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    var ref:FIRDatabaseReference!
    var storage:FIRStorageReference!
    var images:[String] = []
    var imagePicker:UIImagePickerController!
    @IBOutlet var collectionView:UICollectionView!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        ref = FIRDatabase.database().reference()
        storage = FIRStorage.storage().reference(forURL: "gs://workshop-cb1ef.appspot.com")

        ref.child("images").queryOrderedByKey().observe(.value, with: {snapshot in
            
            let dict = snapshot.children.allObjects.reversed()
            
            self.images = dict.map({ snap in
                
                let img = snap as! FIRDataSnapshot
                return img.value! as! String
            })
            
            self.collectionView.reloadData()
            self.collectionView.scrollToItem(at: IndexPath(row:0,section:0), at: .top, animated: true)
        
        }, withCancel: {error in
            print("error \(error)")
        })
    }
    
    //MARK: IMAGE PICKER
    @IBAction func takePhoto(sender: UIButton) {
        
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        imagePicker.dismiss(animated: true, completion: nil)
    
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
           
            let data: Data = UIImageJPEGRepresentation(image, 0.7)!
            
            let pushKey = ref.child("images").childByAutoId().key

            let imgRef = storage.child(pushKey)
            
            imgRef.put(data, metadata: nil) { metadata, error in
                if (error != nil) {
                    
                } else {
                    self.ref.child("images").child(pushKey).setValue(pushKey)
                }
            }
        }
    }

//MARK: COLLECTION VIEW DELEGATE
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "photo", for: indexPath) as! PhotoCollectionViewCell
        
        let imgRef = storage.child("\(images[indexPath.row])")
        
        cell.imageView.sd_setImage(with: imgRef, placeholderImage: UIImage(named:"place"))
        cell.imageView.contentMode = .scaleAspectFit
        
        return cell
    }

}

